import React from 'react'
import PropTypes from 'prop-types'

const Ola = ({nome}) => {
    return <span>Ola, {nome}</span>
}

// class Ola extends React.Component {
//     render() {
//         return (
//             <span>Ola, {this.props.nome}</span>
//         )
//     }
// }

Ola.propTypes = {
    nome: PropTypes.string.isRequired
}

Ola.defaultProps = {
    nome: 'José'
}

export default Ola