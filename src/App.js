import React, { Component } from 'react'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import DisplayCounter from './DisplayCounter'

import Ola from './Ola'
import Counter from './Counter'
import counterReducer from './Reducer'
import logger from 'redux-logger'

let store = createStore(counterReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(logger))

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <Counter />
          <DisplayCounter />
          <Ola />
        </div>
      </Provider>
    )
  }
}

export default App;
