import React from 'react'
import { connect } from 'react-redux'
import { increment, decrement } from './Actions'

export const Counter = ({count, increment, decrement}) => {
    return (
        <p>
            <button className='incr' onClick={() => increment(2)}>+</button>
            Contador: <span className='contador'>{count}</span>
            <button className='decr' onClick={() => decrement(1)}>-</button>
        </p>
    )
}

// class Counter extends React.Component {
//     render() {
//         return (
//             <div>
//                 <button className="btn" onClick={this.props.increment}>+</button>
//                 Contador atual: {this.props.count}
//                 <button className="btn" onClick={this.props.decrement}>-</button>
//             </div>
//         )
//     }
// }

const mapStateToProps = (state) => {
    return {
        count: state.count
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        increment: (value) => dispatch(increment(value)),
        decrement: (value) => dispatch(decrement(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter)