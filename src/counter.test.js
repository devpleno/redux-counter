import React from 'react'
import { shallow, configure } from 'enzyme'
import { Counter } from './Counter'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('Counter', () => {
    it('montar', () => {

        const props = {
            count: 10,
            increment: () => 0,
            decrement: () => 0
        }

        const wrapper = shallow(<Counter {...props} />)

        expect(wrapper.length).toEqual(1)
    })

    it('chamar increment', () => {
        const props = {
            count: 10,
            increment: jest.fn(),
            decrement: jest.fn()
        }

        const wrapper = shallow(<Counter {...props} />)

        // Zero increment
        expect(props.increment.mock.calls.length).toEqual(0)

        // Após um clique
        wrapper.find('.incr').simulate('click')
        expect(props.increment.mock.calls.length).toEqual(1)

        // Após outro clique
        wrapper.find('.incr').simulate('click')
        wrapper.find('.incr').simulate('click')
        expect(props.increment.mock.calls.length).toEqual(3)

        // Zero decrement
        expect(props.decrement.mock.calls.length).toEqual(0)

        // Após clique clique
        wrapper.find('.decr').simulate('click')
        expect(props.decrement.mock.calls.length).toEqual(1)

        expect(wrapper.find('.contador').text()).toEqual('10')
    })
})