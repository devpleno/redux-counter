import React from 'react'
import { shallow, configure } from 'enzyme'
import { DisplayCounter } from './DisplayCounter'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('DisplayCounter', () => {
    it('montar', () => {
        const wrapper = shallow(<DisplayCounter count={10} />)
        expect(wrapper.contains(<p>O contador atual é: 10</p>)).toBe(true)
    })
})