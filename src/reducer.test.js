import counterReducer from './Reducer'

test('reducer null', () => {

    const actionMock = { type: 'INCREMENT', value: 5 }

    const state = counterReducer(undefined, actionMock);

    expect(state).toEqual({ count: 5 })
})

test('reducer increment', () => {

    const initialState = {
        count: 0
    }

    const actionMock = { type: 'INCREMENT', value: 5 }

    const state = counterReducer(initialState, actionMock);

    expect(state).toEqual({ count: 5 })
})

test('reducer decrement', () => {

    const initialState = {
        count: 10
    }

    const actionMock = { type: 'DECREMENT', value: 7 }

    const state = counterReducer(initialState, actionMock);

    expect(state).toEqual({ count: 3 })
})